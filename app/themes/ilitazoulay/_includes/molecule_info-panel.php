<div class="info-panel col-md-6 col-xs-10">
  <div class="content-container">
    <a href="#" class="close">Close</a>
    <h2><?php echo $title; ?></h2>
    <p><?php echo $text; ?></p>
    <?php
      if(!empty($credits)):
        echo "<h3>Credits</h3>";
        echo $credits; 
      endif;
    ?>    
  </div>
</div>