<?php
  $i = 0;
  $args_publications = array(
    'numberposts' => -1,
    'post_type'   => 'publications',
  );
  $the_query_publications = new WP_Query($args_publications);

  $args_news = array(
    'numberposts' => -1,
    'post_type'   => 'press',
  );
  $the_query_news = new WP_Query($args_news);
?>

  <main>
      <div class="container-fluid">
         <div class="row sticky-container---">
            <div class="col-md-4 col-md-offset-1 block-publications">
              <div class="sticky-column">
                 <div class="block block-headline-text ">
                  <hr>
                  <h2 class="b-title">News</h2>
                </div>
                <?php
                  if($the_query_news->have_posts()):
                  while ( $the_query_news->have_posts() ) : $the_query_news->the_post();
                    $title           = get_the_title();
                    $link            = get_permalink();
                    $excerpt         = nl2br(get_the_excerpt());
                    $image_id        = get_post_thumbnail_id();
                    $image_array     = wp_get_attachment_image_src($image_id , "medium");
                    $image_url       = $image_array[0];
                    $image_width     = $image_array[1];
                    $image_height    = $image_array[2];
                ?>
                <article>
                  <div class="block block-headline-byline <?php echo $class_article; ?>">
                    <?php if(!empty($ext_link)): ?>
                      <a href="<?php echo $ext_link; ?>" target="_blank">
                    <?php else: ?>
                      <a href="<?php echo $link; ?>">
                    <?php endif; ?>
                      <h2 class="headline"><?php echo $title; ?></h2>
                    </a>
                    <p class="excerpt"><?php echo $excerpt; ?></p>
                  </div>
                </article>
                <?php endwhile; ?>
                <?php endif; ?>
               </div>
            </div>
            <div class="col-md-4 col-md-offset-2 block-press">
              <div class="sticky-column">
                 <div class="block block-headline-byline">
                  <hr>
                  <h2 class="b-title">Publications</h2>
                </div>
                <?php
                  if($the_query_publications->have_posts()):
                  while ( $the_query_publications->have_posts() ) : $the_query_publications->the_post();
                    $title           = get_the_title();
                    $link            = get_permalink();
                    $image_id        = get_post_thumbnail_id();
                    $image_array     = wp_get_attachment_image_src($image_id , "medium");
                    $image_url       = $image_array[0];
                    $image_width     = $image_array[1];
                    $image_height    = $image_array[2];
                ?>
                  <article class="block-text col-md-10 col-md-offset-1">
                    <div class="block block-thumb-headline">
                      <a href="<?php echo $link; ?>" class="b-inner">
                        <?php if(!empty($image_url)): ?>
                        <div class="b-thumb">
                          <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>">
                        </div>
                        <?php endif; ?>
                        <div class="b-text">
                          <h2 class="b-title"><?php echo $title; ?></h2>
                        </div>
                      </a>
                    </div>
                  </article>
                <?php endwhile; ?>
                <?php endif; ?>
               </div>
            </div>
         </div>
      </div>
   </main>
