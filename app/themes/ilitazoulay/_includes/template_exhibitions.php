<?php
  $i = 1;
  $args_news = array(
    'numberposts' => -1,   
    'post_type'   => 'exhibitions',   
  );
  $the_query = new WP_Query($args_news);
?>
<main>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-1">
        <div class="block block-headline-byline">
          <hr>
          <h2 class="b-title">Recent exhibitions</h2>
        </div>
      </div> 
    </div>
  </div>               
  <?php
  if($the_query->have_posts()):
  while ( $the_query->have_posts() ) : $the_query->the_post();
    $title           = get_the_title();
    $text            = get_the_content();
    $link            = get_permalink();
    $excerpt         = nl2br(get_the_excerpt());
    $image_id        = get_post_thumbnail_id();
    $image_array     = wp_get_attachment_image_src($image_id , "large");
    $image_url       = $image_array[0];
    $image_width     = $image_array[1]; 
    $image_height    = $image_array[2];
    $external_link   = get_field('external_link');
    $class_article   = (!empty($external_link))? "external " : "";
    $class_article   .= (empty($image_url))? "img-empty " : "";
  ?>
  <article>
    <?php if($i < 0): ?>
    <div class="block block-thumb container-fluid block-thumb-left <?php echo $class_article; ?>">
      <?php if(!empty($external_link)): ?>
      <a href="<?php echo $external_link; ?>" target="_blank" class="b-inner row">
      <?php elseif(!empty($text)): ?>
      <a href="<?php echo $link; ?>" class="b-inner row">
    <?php else: ?>
      <div class="b-inner row">
      <?php endif; ?>        
        <div class="b-text col-sm-4 col-sm-offset-1">
           <h2 class="b-title"><?php echo $title; ?></h2>
           <p><?php echo $excerpt; ?></p>
        </div>
        <div class="b-thumb col-sm-6">
          <?php if(!empty($image_url)):  ?>
            <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>">
          <?php endif; ?>          
        </div>
      <?php if(!empty($text) or !empty($external_link)): ?>
      </a>
      <?php else: ?>
      </div>
      <?php endif; ?>  
    </div>
    <?php else: ?>
      <div class="block block-thumb container-fluid block-thumb-right <?php echo $class_article; ?>">
        <?php if(!empty($external_link)): ?>
        <a href="<?php echo $external_link; ?>" target="_blank" class="b-inner row">
        <?php elseif(!empty($text)): ?>
        <a href="<?php echo $link; ?>" class="b-inner row">
        <?php else: ?>
        <div class="b-inner row">
        <?php endif; ?>       
          <div class="b-thumb col-sm-6 col-sm-offset-1">
          <?php if(!empty($image_url)):  ?>
          <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>">
          <?php endif; ?>
          </div>
          <div class="b-text col-sm-4">
            <h2 class="b-title"><?php echo $title; ?></h2>
            <p><?php echo $excerpt; ?></p>
          </div>
        <?php if(!empty($text) or !empty($external_link)): ?>
        </a>
        <?php endif; ?>      
      </div>
    <?php endif; ?>
  </article>
  <?php $i *= -1; endwhile; ?>
  <?php endif; ?>
</main>


