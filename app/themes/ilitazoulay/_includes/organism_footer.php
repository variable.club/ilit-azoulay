<!-- Begin Footer -->
<footer class="footer container-fluid" role="contentinfo">
  <div class="row lc">
    <div class="col-sm-11 col-sm-offset-1">
      <p class="copyright">&copy; 2015  Ilit Azoulay. All rights reserved.</p>
      <p class="credit">Website design & development by <a href="http://www.variable.club" targer="_blank">Variable</a>.</p>
    </div>
  </div>
</footer>
<!-- End Footer -->
<script src="<?php echo site_url(); ?>/assets/dist/scripts<?php echo IS_MINIFIED; ?>.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69928114-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>