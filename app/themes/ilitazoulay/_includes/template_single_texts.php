<?php
  while (have_posts()) : the_post();
  $post_id          = $post->ID;
  $title            = esc_html(get_the_title());
  $text             = get_the_content();
  $slug             = $post->post_name;
  $intro            = get_field("intro");
  $author           = get_field("author");
  $published        = get_field("published");
  $translation      = get_field("translation");
  $image_id        = get_post_thumbnail_id();
  $image_array     = wp_get_attachment_image_src($image_id , "medium");
  $image_url       = $image_array[0];
  $image_width     = $image_array[1];
  $image_height    = $image_array[2];
?>
<main class="single">
  <article class="container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-1">
        <div class="block block-headline-byline">
          <hr>
          <h2 class="b-title"><?php echo $title; ?></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <ul>
          <?php if(!empty($author)): ?>
          <li>by <?php echo $author; ?></li>
          <?php endif; ?>
          <?php if(!empty($published)): ?>
          <li>Published in:&nbsp;<?php echo $published; ?></li>
          <?php endif; ?>
          <?php if(!empty($translation)): ?>
          <li>Read the <a href="<?php echo $translation; ?>" target="_blank">Hebrew Translation</a></li>
          <?php endif; ?>
        </ul><br><br>
        <div class="intro">
          <?php echo $intro; ?>
        </div>
        <?php echo $text; ?>
        <?php if(!empty($image_url)): ?>
          <figure>
            <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>">
          </figure>
        <?php endif; ?>
    </div>
    </div>
  </article>
</main>
<?php endwhile; ?>
