<?php
$search_query = (is_search())? get_search_query() : "";
?>
<!DOCTYPE html>

<!--[if IE 6]><html class="no-js ie6" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]><html class="no-js ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="no-js ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php wp_title(''); ?></title>
  <meta name="viewport" content="width=device-width, maximum-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta property="fb:admins" content="569403070">
  <meta property="fb:app_id" content="307156226006266">
  <link rel="stylesheet" href="<?php echo site_url(); ?>/assets/dist/screen<?php echo IS_MINIFIED; ?>.css">
  <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="msapplication-TileImage" content="/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Begin .header -->
<header class="header cf container-fluid" role="banner">
   <div class="row">
      <div class="col-sm-4 col-sm-offset-1">
         <a href="<?php echo site_url(); ?>" alt="Ilit Azoulay" class="logo"><h1>Ilit Azoulay</h1></a></div>
      <div class="col-sm-4 col-sm-offset-2">
         <nav id="nav" class="nav">
          <ul class="nav-list">
            <?php
             $primary_nav = array(
             'theme_location'  => '',
             'menu'            => 'Main',
             'container'       => '',
             'container_class' => '',
             'container_id'    => '',
             'menu_class'      => 'primary',
             'menu_id'         => '',
             'echo'            => true,
             'fallback_cb'     => '',
             'before'          => '',
             'after'           => '',
             'link_before'     => '',
             'link_after'      => '',
             'items_wrap'      => '<ul class="nav-list">%3$s</ul>',
             'depth'           => 0,
             //'walker'          => new themeslug_walker_nav_menu
            );
            wp_nav_menu($primary_nav);
            ?>  
          </ul>
         </nav><!--end .nav-->
      </div>
   </div>
</header>
<!-- End .header -->