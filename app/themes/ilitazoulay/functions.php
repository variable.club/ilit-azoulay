<?php
/**
 * Theme setup
 * 
 */
require(get_template_directory() . '/functions/theme-setup.php');

// Altering queries
require(get_template_directory() . '/functions/queries.php');

// Admin scripts
require(get_template_directory() . '/functions/admin.php');

// Champs Advanced Custom Fields 
require(get_template_directory() . '/functions/acf-export.php');


/**
 * Global var
 * 
 */
global  
  $siteurl,
  $patternlab_url,
  $patternlab_images_url,
  $banners_url;


$patternlab_url = SITEURL."/patternlab/";
$patternlab_images_url = get_template_directory_uri()."/images/";
$gallery_slide_num = 0;