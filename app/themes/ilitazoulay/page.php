<?php
//Header
require_once('_includes/organism_header.php');

// Page Projects
if (is_front_page()):
  require_once('_includes/template_homepage.php');

// Page Publications
elseif(is_page('news-publications')):
  require_once('_includes/template_news-publications.php');


// Page Textes
elseif(is_page('texts')):
  require_once('_includes/template_texts.php');

// // Page Publications
// elseif(is_page('publications-press')):
//   require_once('_includes/template_press-publications.php');

// Page News
elseif(is_page('exhibitions')):
  require_once('_includes/template_exhibitions.php');

// Infos-contact
elseif(is_page('contact-infos')):
  require_once('_includes/template_page-infos.php');
endif;

//Footer
require_once('_includes/organism_footer.php');
