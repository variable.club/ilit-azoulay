<?php

/**
 * modify the category query 
 */

add_action( 'pre_get_posts', 'modify_category_query' );

function modify_category_query( $query ) {
  if ($query->is_category() && $query->is_main_query() ) {
    $query->query_vars['ignore_sticky_posts'] = 1;
     $query->set('orderby', 'date');
     $query->set('order', 'DESC' );
  }
}
