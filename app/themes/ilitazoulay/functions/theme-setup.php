<?php

/**
 * Ajout styles autours des images 
 */

add_filter( 'image_send_to_editor', 'wp_image_wrap_init', 10, 8 );    
  function wp_image_wrap_init( $html, $id, $caption, $title, $align, $url, $size, $alt ) {
  return '<figure class="image">'. $html .'</figure>';
}

/**
 * Suppression de crasses Wordpress
 */

add_action('wp_print_styles','dequeue_styles');
function dequeue_styles(){
  wp_dequeue_style('yarppWidgetCss');
  wp_deregister_style('yarppRelatedCss');
}

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

add_action( 'wp_print_scripts', 'wpdocs_dequeue_script');
function wpdocs_dequeue_script() {
  wp_dequeue_script( 'jquery' );
}


/**
 * Images sizes (hérité de l'ancien site)
 */

//add_image_size('ios-fullscreen', 1024, 1024); //Desktop Large Homepage, Desktop Large Index; Desktop Articles 
//add_image_size('rss', 400); //Desktop Medium homepage, Desktop Index all


/**
 * Return a clean human url (without "http://" and trailing "/")
 */
function make_human_url($url){
  $url = str_replace("http://", "", $url);
  $url = str_replace("https://", "", $url);
  $url = rtrim($url, "/");
  return $url;
}


