<?php

/**
 * Champs Advance Custom Fields 
 * Créé les champs en php sur le serveur:
 * En local, ces champs sont créés via l'interface d'admin et exportés dans ce fichier:
 * Custom Fiels > Export > Export to PHP 
 */
// if(strstr($_SERVER["HTTP_HOST"], "ilitazoulay.dev") != true){

   if(function_exists("register_field_group"))
  {
    register_field_group(array (
      'id' => 'acf_texts',
      'title' => 'Texts',
      'fields' => array (
        array (
          'key' => 'field_5620c35642dc1',
          'label' => 'Intro',
          'name' => 'intro',
          'type' => 'textarea',
          'default_value' => '',
          'placeholder' => '',
          'maxlength' => '',
          'rows' => '',
          'formatting' => 'br',
        ),
        array (
          'key' => 'field_5620adeeba661',
          'label' => 'Author',
          'name' => 'author',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5620ae01ba662',
          'label' => 'Hebrew translation',
          'name' => 'translation',
          'type' => 'file',
          'save_format' => 'url',
          'library' => 'all',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'texts',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'side',
        'layout' => 'default',
        'hide_on_screen' => array (
          0 => 'custom_fields',
          1 => 'discussion',
          2 => 'comments',
          3 => 'revisions',
          4 => 'slug',
          5 => 'author',
          6 => 'format',
          7 => 'featured_image',
          8 => 'categories',
          9 => 'tags',
          10 => 'send-trackbacks',
        ),
      ),
      'menu_order' => -1,
    ));
    register_field_group(array (
      'id' => 'acf_press-news',
      'title' => 'Press & news',
      'fields' => array (
        array (
          'key' => 'field_56211d7149a32',
          'label' => 'External link',
          'name' => 'external_link',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'press',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'exhibitions',
            'order_no' => 0,
            'group_no' => 1,
          ),
        ),
      ),
      'options' => array (
        'position' => 'acf_after_title',
        'layout' => 'default',
        'hide_on_screen' => array (
          0 => 'custom_fields',
          1 => 'discussion',
          2 => 'comments',
          3 => 'revisions',
          4 => 'author',
          5 => 'format',
          6 => 'categories',
          7 => 'tags',
          8 => 'send-trackbacks',
        ),
      ),
      'menu_order' => 0,
    ));
    register_field_group(array (
      'id' => 'acf_projects',
      'title' => 'Projects',
      'fields' => array (
        array (
          'key' => 'field_5613d13eff5ba',
          'label' => 'Credits',
          'name' => 'credits',
          'type' => 'wysiwyg',
          'default_value' => '',
          'toolbar' => 'full',
          'media_upload' => 'yes',
        ),
        array (
          'key' => 'field_5613d379baa6b',
          'label' => 'Year',
          'name' => 'year',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5613e0edc1e3f',
          'label' => 'Gallery type',
          'name' => 'gallery_type',
          'type' => 'select',
          'choices' => array (
            'Lightbox' => 'Lightbox',
            'Horizontal slide' => 'Horizontal slide',
            'Grow' => 'Grow',
          ),
          'default_value' => '',
          'allow_null' => 0,
          'multiple' => 0,
        ),
        array (
          'key' => 'field_5613d2e54c125',
          'label' => 'Gallery alignment',
          'name' => 'gallery_alignment',
          'type' => 'select',
          'conditional_logic' => array (
            'status' => 1,
            'rules' => array (
              array (
                'field' => 'field_5613e0edc1e3f',
                'operator' => '==',
                'value' => 'Lightbox',
              ),
            ),
            'allorany' => 'all',
          ),
          'choices' => array (
            'Top' => 'Top',
            'Bottom' => 'Bottom',
          ),
          'default_value' => 'Top',
          'allow_null' => 0,
          'multiple' => 0,
        ),
        array (
          'key' => 'field_5622689eb9528',
          'label' => 'Gallery alignment horizontal',
          'name' => 'gallery_alignment_horizontal',
          'type' => 'select',
          'conditional_logic' => array (
            'status' => 1,
            'rules' => array (
              array (
                'field' => 'field_5613e0edc1e3f',
                'operator' => '==',
                'value' => 'Grow',
              ),
            ),
            'allorany' => 'all',
          ),
          'choices' => array (
            'Left' => 'Left',
            'Center' => 'Center',
          ),
          'default_value' => 'Left',
          'allow_null' => 0,
          'multiple' => 0,
        ),
        array (
          'key' => 'field_56226853979c8',
          'label' => 'Gallery alignment (grow)',
          'name' => 'gallery_alignment_grow',
          'type' => 'select',
          'conditional_logic' => array (
            'status' => 1,
            'rules' => array (
              array (
                'field' => 'field_5613e0edc1e3f',
                'operator' => '==',
                'value' => 'Grow',
              ),
            ),
            'allorany' => 'all',
          ),
          'choices' => array (
            'Top' => 'Top',
            'Middle' => 'Middle',
            'Bottom' => 'Bottom',
          ),
          'default_value' => 'Top',
          'allow_null' => 0,
          'multiple' => 0,
        ),
        array (
          'key' => 'field_5613d3134c126',
          'label' => 'Margins',
          'name' => 'margins',
          'type' => 'select',
          'conditional_logic' => array (
            'status' => 1,
            'rules' => array (
              array (
                'field' => 'field_5613e0edc1e3f',
                'operator' => '==',
                'value' => 'Lightbox',
              ),
              array (
                'field' => 'field_5613e0edc1e3f',
                'operator' => '==',
                'value' => 'Grow',
              ),
            ),
            'allorany' => 'any',
          ),
          'choices' => array (
            'No' => 'No',
            'Small' => 'Small',
            'Medium' => 'Medium',
            'Large' => 'Large',
            'Extra-large' => 'Extra-large',
          ),
          'default_value' => 'Medium',
          'allow_null' => 0,
          'multiple' => 0,
        ),
        array (
          'key' => 'field_5613d1fefb464',
          'label' => 'Gallery',
          'name' => 'gallery',
          'type' => 'repeater',
          'sub_fields' => array (
            array (
              'key' => 'field_5613d232fb466',
              'label' => 'Picture',
              'name' => 'picture',
              'type' => 'image',
              'column_width' => '',
              'save_format' => 'object',
              'preview_size' => 'thumbnail',
              'library' => 'all',
            ),
            array (
              'key' => 'field_5613d216fb465',
              'label' => 'Legend',
              'name' => 'legend',
              'type' => 'text',
              'column_width' => 70,
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_5613d26cfb467',
              'label' => 'Width',
              'name' => 'width',
              'type' => 'number',
              'column_width' => 15,
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => 'cm',
              'min' => '',
              'max' => '',
              'step' => '',
            ),
            array (
              'key' => 'field_5613d29bfb468',
              'label' => 'Height',
              'name' => 'height',
              'type' => 'text',
              'column_width' => 15,
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => 'cm',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_562a07fc9d3b1',
              'label' => 'Sounds',
              'name' => 'sounds',
              'type' => 'repeater',
              'column_width' => '',
              'sub_fields' => array (
                array (
                  'key' => 'field_562a08079d3b2',
                  'label' => 'Title',
                  'name' => 'titre',
                  'type' => 'text',
                  'column_width' => '',
                  'default_value' => '',
                  'placeholder' => '',
                  'prepend' => '',
                  'append' => '',
                  'formatting' => 'html',
                  'maxlength' => '',
                ),
                array (
                  'key' => 'field_562a08119d3b3',
                  'label' => 'MP3',
                  'name' => 'mp3',
                  'type' => 'file',
                  'column_width' => '',
                  'save_format' => 'object',
                  'library' => 'all',
                ),
              ),
              'row_min' => '',
              'row_limit' => '',
              'layout' => 'row',
              'button_label' => 'Add Row',
            ),
          ),
          'row_min' => '',
          'row_limit' => '',
          'layout' => 'row',
          'button_label' => 'Add Row',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'post',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array (
          0 => 'permalink',
          1 => 'excerpt',
          2 => 'custom_fields',
          3 => 'discussion',
          4 => 'comments',
          5 => 'revisions',
          6 => 'author',
          7 => 'format',
          8 => 'featured_image',
          9 => 'categories',
          10 => 'tags',
          11 => 'send-trackbacks',
        ),
      ),
      'menu_order' => 0,
    ));
    register_field_group(array (
      'id' => 'acf_publications',
      'title' => 'Publications',
      'fields' => array (
        array (
          'key' => 'field_5620fbcaad5f4',
          'label' => 'Introduction',
          'name' => 'introduction',
          'type' => 'textarea',
          'default_value' => '',
          'placeholder' => '',
          'maxlength' => '',
          'rows' => '',
          'formatting' => 'br',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'publications',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'acf_after_title',
        'layout' => 'no_box',
        'hide_on_screen' => array (
        ),
      ),
      'menu_order' => 0,
    ));
    register_field_group(array (
      'id' => 'acf_publications-bottom',
      'title' => 'Publications bottom',
      'fields' => array (
        array (
          'key' => 'field_5620fc11e817f',
          'label' => 'Informations',
          'name' => 'infos',
          'type' => 'wysiwyg',
          'default_value' => '',
          'toolbar' => 'full',
          'media_upload' => 'yes',
        ),
        array (
          'key' => 'field_5620fc3be8180',
          'label' => 'Where to get it ?',
          'name' => 'where',
          'type' => 'wysiwyg',
          'default_value' => '',
          'toolbar' => 'full',
          'media_upload' => 'yes',
        ),
        array (
          'key' => 'field_5620fc53e8181',
          'label' => 'Pictures',
          'name' => 'pictures',
          'type' => 'repeater',
          'sub_fields' => array (
            array (
              'key' => 'field_5620fc60e8182',
              'label' => 'Image file',
              'name' => 'image_file',
              'type' => 'image',
              'column_width' => '',
              'save_format' => 'object',
              'preview_size' => 'thumbnail',
              'library' => 'all',
            ),
          ),
          'row_min' => '',
          'row_limit' => '',
          'layout' => 'table',
          'button_label' => 'Add Row',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'publications',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array (
        ),
      ),
      'menu_order' => 0,
    ));
  }

  if(function_exists("register_field_group"))
  {
    register_field_group(array (
      'id' => 'acf_infos-contact',
      'title' => 'Infos & Contact',
      'fields' => array (
        array (
          'key' => 'field_5631f474e6395',
          'label' => 'Infos',
          'name' => 'infos',
          'type' => 'wysiwyg',
          'default_value' => '',
          'toolbar' => 'full',
          'media_upload' => 'yes',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'page',
            'operator' => '==',
            'value' => '221',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'normal',
        'layout' => 'default',
        'hide_on_screen' => array (
          0 => 'excerpt',
          1 => 'custom_fields',
          2 => 'discussion',
          3 => 'comments',
          4 => 'revisions',
          5 => 'slug',
          6 => 'author',
          7 => 'format',
          8 => 'featured_image',
          9 => 'categories',
          10 => 'tags',
          11 => 'send-trackbacks',
        ),
      ),
      'menu_order' => 0,
    ));
  }



// }