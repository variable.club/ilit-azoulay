<?php
/*
Single Post Template: Template 2012
*/
?>
<?php

require_once('_includes/organism_header.php');
// Texts
if($post->post_type == 'texts'):
  require_once('_includes/template_single_texts.php');
// Publications
elseif($post->post_type == 'publications'):
  require_once('_includes/template_single_publications.php');
// Press
elseif($post->post_type == 'press'):
  require_once('_includes/template_single_texts.php');
// Exhibition
elseif($post->post_type == 'exhibitions'):
  require_once('_includes/template_single_texts.php');
endif;
require_once('_includes/organism_footer.php');
?>
