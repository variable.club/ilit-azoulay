var vWidth = $(window).width();
window.onresize = function(event) {
  vWidth = $(window).width();
};

jQuery(document).ready(function($){

   // gallery-slide grab when mouse down
   $('.gallery-slide').mousedown(function(){
      $(this).addClass("grab");
   });
      $('.gallery-slide').mouseup(function(){
      $(this).removeClass("grab");
   });

  //Scroll gallery-slide
  $('.gallery-slide .arrow-right').click(function() {
    var $target = $(this).parent().parent();
    $target.scrollTo('+='+vWidth+'px', 800);;
    return false;
  });
  $('.gallery-slide .arrow-left').click(function() {
    var $target = $(this).parent().parent();
    $target.scrollTo('-='+vWidth+'px', 800);;
    return false;
  });

  //Show/hide infos panel
  $('.home article').each(function() {
    more = $(this).find( ".more" );
    var info_panel  = $(this).find('.info-panel');

    more.click(function() {
      info_panel.show();
      $("body").addClass('panel-open');
      return false;
    });
  });

  $(".close, #panel-full").click(function() {
    $('.info-panel').hide();
    $("body").removeClass('panel-open');
    return false;
  });

  // big images on click
  $('.small-to-big a').each(function() {
    $(this).click(function() {
      var parent = $(this).parent().parent();
      if(parent.hasClass('small')){
        resizeOtherImages();
        parent.addClass('big').removeClass('small');
        //Scroll to this
        $(document).scrollTop( parent.offset().top);
      }else{
        parent.removeClass('big').addClass('small');
      }

      return false;
    });
  });

  //packery
  var $container = $('.gallery-lightbox');
  // init
  $container.packery();

  // lazyload
  $("img.lazy").lazyload({
    effect : "fadeIn",
    effectspeed: 1500,
    threshold: 0,
    failure_limit : 50
  });
  //lazyload horizontal : boucle sur les galleries de ce type
  var gallery_slide = 0;
  $('.gallery-slide').each(function() {
    $("img.lazy-h-"+gallery_slide).lazyload({
      container: $(".gallery-slide-"+gallery_slide),
      effect : "fadeIn",
      effectspeed: 1000,
      threshold: 3000,
    });
    gallery_slide++;
  });



  // Display image on click caption
  $(".info-panel figure").click(function() {
    $(this).addClass('display');
    return false;
  });

});

function resizeOtherImages(){
  $('.small-to-big a').each(function() {
    $(this).parent().parent().removeClass('big').addClass('small');
  });
}
